import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    currentCityId: 'omsk',
    currentType: 'c',
    cityList: [
      {
        id: 'omsk',
        name: 'Омск',
        weatherInfo: {
          temperature: {
            c: 19,
            f: 48
          },
          icon: 'sun',
          description: 'Солнечно',
          weatherOptions: [
            {
              name: 'Ветер',
              value: '5 м/c, западный'
            },
            {
              name: 'Давление',
              value: '752 мм рт. ст.'
            },
            {
              name: 'Влажность',
              value: '60%'
            },
            {
              name: 'Вероятность дождя',
              value: '10%'
            }
          ]
        }
      },
      {
        id: 'ekb',
        name: 'Екатеринбург',
        weatherInfo: {
          temperature: {
            c: 14,
            f: 46
          },
          icon: 'cloud',
          description: 'Облачно, возможен дождь',
          weatherOptions: [
            {
              name: 'Ветер',
              value: '15 м/c, северный'
            },
            {
              name: 'Давление',
              value: '732 мм рт. ст.'
            },
            {
              name: 'Влажность',
              value: '80%'
            },
            {
              name: 'Вероятность дождя',
              value: '50%'
            }
          ]
        }
      }
    ]
  },
  getters: {
    getCurrentCity: state => {
      return state.cityList.find(city => city.id === state.currentCityId)
    }
  },
  mutations: {
    setCityId(state, id) {
      state.currentCityId = id
    },
    setType(state, id) {
      state.currentType = id
    }
  }
})

export default store
