module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/scss/_breakpoints.scss";
          @import "@/scss/_mixins.scss";
        `
      }
    }
  }
}